const express = require("express");
const morgan = require("morgan");
const dotenv = require("dotenv");

//Chargement du fichier de configuration (config.env) qui contient le port, l'environnement, l'URL vers le site et la clé API
dotenv.config({ path: "./config.env" });

const app = express();

//Si on est en mode développement
if (process.env.NODE_ENV === "development") {
    //Utilisation de morgan pour fournir les informations sur la requête et les mettre en couleurs
    app.use(morgan("dev"));
}

//Utilisation du fichier profile.js dans le dossier route pour les requêtes
app.use("/api/v1/profile", require("./routes/profile"));

//Lorsque l'application est déployer
if (process.env.NODE_ENV === "production") {
    //Chargement du dossier public (dossier contenant l'application en VueJs)
    app.use(express.static(__dirname + "/public/"));

    //Chargement de la page index.html dans le dossier public
    app.get(/.*/, (req, res) => res.sendFile(__dirname + "/public/index.html"));
}

//Création d'une constante qui récupere le port dans le fichier config ou qui utilise le port 8000 si celui-ci n'est pas trouvé
const port = process.env.PORT || 8000;

app.listen(port, () => {
    console.log(
        `Server running in ${process.env.NODE_ENV} mode on port ${port}`
    );
});
