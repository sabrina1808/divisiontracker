const express = require("express");
const router = express.Router();
//Permet de faire des appels de requêtes API depuis le back-end
const fetch = require("node-fetch");

router.get("/:platform/:gamertag", async (req, res) => {
    try {
        const headers = {
            "TRN-Api-Key": process.env.TRACKER_API_KEY,
        };
        //Enregistrement des paramètres de la requêtes dans une constante
        const { platform, gamertag } = req.params;

        console.log(req.params.platform, req.params.gamertag);

        //Définition d'une constante qui contiendra la réponse de la requête API
        const response = await fetch(
            `${process.env.TRACKER_API_URL}/profile/${platform}/${gamertag}`,
            {
                headers,
            }
        );

        //Conversion du format de la constante response au format json
        const respData = await response.json();

        //On vérifie que la réponse que l'on obtient ne contient pas d'erreur
        if (respData.errors && respData.errors.length > 0) {
            //On renvoie une réponse au status 404 et on affiche le message disant que le profil n'a pas été trouvé
            return res.status(404).json({
                message: "Profile not found...",
            });
        }

        //Envoi des données de la réponse au format json
        res.json(respData);
    } catch (err) {
        //Affichage de l'erreur dans la console
        console.error(err);
        //Envoi d'une reponse au status 500 avec un message d'erreur
        res.status(500).json({
            message: "Server Error",
        });
    }
});

//On exporte le router pour avec la route pour le requête
module.exports = router;
