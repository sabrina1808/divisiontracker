import Vue from 'vue';
import App from './App.vue';
import {router} from './helper/router';
import VueToasted from 'vue-toasted';

Vue.config.productionTip = false

//Chargement de Vue-Toasted
Vue.use(VueToasted);

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
