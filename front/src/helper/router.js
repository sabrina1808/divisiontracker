import Vue from 'vue';
import VueRouter from 'vue-router';
import Search from '../components/Search.vue';
import Profile from '../components/Profile.vue';

Vue.use(VueRouter);

const routes = [
    {path: '/', component: Search},
    {path: '/profile/:platform/:gamertag', component: Profile}
];

//on exporte par défault le router
export const router = new VueRouter({
    routes: routes
});