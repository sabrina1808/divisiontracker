const path = require('path');

module.exports = {
    outputDir: path.resolve(__dirname, '../public'), //Permet de rajouter le dossier public dans le dossier node et non vue pour la mise en ligne du projet
    devServer: {
        proxy: {
            '/api/v1': {
                target: 'http://localhost:5000'
            }
        }
    }
};